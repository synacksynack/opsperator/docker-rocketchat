'use strict';

let MongoClient;
try { MongoClient = require('/app/bundle/programs/server/npm/node_modules/mongodb').MongoClient; }
catch(e) { MongoClient = require('/app/bundle/programs/server/npm/node_modules/meteor/npm-mongo/node_modules/mongodb').MongoClient; }

const dbName = process.env.MONGO_DB || 'rocket';
const debug = process.env.DEBUG !== undefined;
const settingsCollection = 'rocketchat_settings';
const url = 'mongodb://'+process.env.MONGO_USER+':'+process.env.MONGO_PASS+'@'+process.env.MONGO_HOST+':27017/'+process.env.MONGO_DB;

// mongodb://$MONGO_USER:$MONGO_PASS@$MONGO_HOST:27017/$MONGO_DB

console.log("Initializing RocketChat site configuration");
if (debug !== undefined) { console.log(url); }

try {
    MongoClient.connect(url, (err, client) => {
	    if (err !== null) {
		console.log('Failed connecting to MongoDB');
		process.exit(1);
	    }
	    console.log("Connected successfully to server");

	    try {
		const db = client.db(dbName);
		db.collection(settingsCollection)
		    .find({ })
		    .toArray((e, d) => {
			    if (e !== null) {
				console.log(e);
				process.exit(1);
			    } else {
				for (let i = 0; i < d.length; i++) {
				    console.log(d[i]);
				}
				process.exit(0);
			    }
			});
	    } catch(e) { console.log(e); process.exit(1); }
	});
} catch(e) {
    console.log("Could not initiate link to MongoDB");
    process.exit(1);
}
