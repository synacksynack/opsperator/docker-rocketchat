apiVersion: apps/v1
kind: Deployment
metadata:
  name: rocket-kube
  namespace: ci
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 3
  selector:
    matchLabels:
      name: rocket-kube
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      labels:
        name: rocket-kube
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: name
                operator: In
                values:
                - rocket-kube
            topologyKey: kubernetes.io/hostname
      containers:
      - env:
        - name: ADMIN_PASS
          valueFrom:
            secretKeyRef:
              key: admin-password
              name: rocket-kube
        - name: ADMIN_USERNAME
          valueFrom:
            secretKeyRef:
              key: admin-user
              name: rocket-kube
        - name: Accounts_AvatarStorePath
          value: /app/uploads
        - name: DEBUG
          value: yay
        - name: LLNG_PROTO
          value: https
        - name: LLNG_SSO_DOMAIN
          value: auth.ci.apps.intra.unetresgrossebite.com
        - name: MAIL_URL
          value: smtp://smtp.vms.intra.unetresgrossebite.com
        - name: MONGO_DB
          valueFrom:
            secretKeyRef:
              key: database-name
              name: rocket-mongodb-kube
        - name: MONGO_HOST
          value: rocket-mongodb-kube-ready
        - name: MONGO_PASS
          valueFrom:
            secretKeyRef:
              key: database-password
              name: rocket-mongodb-kube
        - name: MONGO_REPLICA_NAME
          valueFrom:
            secretKeyRef:
              key: database-replicaname
              name: rocket-mongodb-kube
        - name: MONGO_USER
          valueFrom:
            secretKeyRef:
              key: database-user
              name: rocket-mongodb-kube
        - name: NODE_ENV
          value: production
        - name: OPENLDAP_BASE
          valueFrom:
            secretKeyRef:
              key: directory-root
              name: openldap-kube
        - name: OPENLDAP_BIND_DN_PREFIX
          value: cn=rocket,ou=services
        - name: OPENLDAP_BIND_PW
          valueFrom:
            secretKeyRef:
              key: rocket-password
              name: openldap-kube
        - name: OPENLDAP_DOMAIN
          value: ci.apps.intra.unetresgrossebite.com
        - name: OPENLDAP_HOST
          value: openldap-kube.ci.svc.cluster.local
        - name: OPENLDAP_PORT
          value: "1389"
        - name: OPENLDAP_PROTO
          value: ldap
        - name: PROMETHEUS_ENABLED
          value: yay
        - name: PROMETHEUS_PORT
          value: "9113"
        - name: PORT
          value: "3000"
        - name: PUBLIC_PROTO
          value: https
        - name: ROCKET_COUNTRY
          value: france
        - name: ROCKET_DOMAIN
          value: chat.ci.apps.intra.unetresgrossebite.com
        - name: ROCKET_INDUSTRY
          value: blockchain
        - name: ROCKET_LANG
          value: en
        - name: ROCKET_ORG_NAME
          value: Opsperator
        - name: ROCKET_ORG_TYPE
          value: enterprise
        - name: ROCKET_SAML_URL
          value: https://auth.ci.apps.intra.unetresgrossebite.com
        - name: ROCKET_SERVER_TYPE
          value: privateTeam
        - name: ROCKET_SITE_NAME
          value: KubeChat
        - name: ROCKET_WEB_MAX_RETRY
          value: "60"
        - name: ROCKET_INIT_MAX_RETRY
          value: "60"
        - name: TZ
          value: Europe/Paris
        image: registry.gitlab.com/synacksynack/opsperator/docker-rocket:master
        imagePullPolicy: Always
        livenessProbe:
          failureThreshold: 118
          httpGet:
            path: /api/info
            port: 3000
            scheme: HTTP
          initialDelaySeconds: 60
          periodSeconds: 30
          successThreshold: 1
          timeoutSeconds: 5
        name: rocket
        ports:
        - containerPort: 3000
          protocol: TCP
        - containerPort: 9113
          protocol: TCP
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /api/info
            port: 3000
            scheme: HTTP
          initialDelaySeconds: 30
          periodSeconds: 15
          successThreshold: 1
          timeoutSeconds: 3
        resources:
          limits:
            cpu: 600m
            memory: 1280Mi
          requests:
            cpu: 100m
            memory: 768Mi
        volumeMounts:
        - mountPath: /certs
          name: pkiconf
        - mountPath: /app/uploads
          name: data
        - mountPath: /etc/ldap
          name: ldapconf
      volumes:
      - emptyDir: {}
        name: data
      - configMap:
          defaultMode: 420
          name: openldap-kube-config
        name: ldapconf
      - name: pkiconf
        projected:
          defaultMode: 420
          sources:
          - secret:
              items:
              - key: ca.crt
                path: ca.crt
              name: kube-root-ca
