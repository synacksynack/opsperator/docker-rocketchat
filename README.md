# k8s Rocket

Forked from https://github.com/RocketChat/Docker.Official.Image

Depends on MongoDB.

Build with:
```
$ make build
```

Depends on LDAP

Test with:
```
$ make run
```

Start Demo in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

See https://docs.rocket.chat/guides/administrator-guides/connectivity-services
setting up your push notifications account.

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name               |    Description                           | Default                                                            |
| :----------------------------- | ---------------------------------------- | ------------------------------------------------------------------ |
|  `ADMIN_PASS`                  | Rocket.Chat Admin Password               | undef                                                              |
|  `ADMIN_USERNAME`              | Rocket.Chat Admin Username               | `rocket`                                                           |
|  `DONT_WAIT_FOR_MONGO`         | Start Rocket.Chat discarding mg check    | undef                                                              |
|  `HOME`                        | Rocket.Chat runtime directory            | `/tmp`                                                             |
|  `MATOMO_ADDITIONAL_TRACKERS`  | Matomo Additional Trackers               | undef                                                              |
|  `MATOMO_COOKIE_DOMAIN`        | Matomo Cookie Domain                     | undef                                                              |
|  `MATOMO_DOMAINS`              | Matomo Domains                           | undef                                                              |
|  `MATOMO_PREPEND_DOMAIN`       | Matomo Prepend Domain                    | undef                                                              |
|  `MATOMO_SITE_ID`              | Matomo Site ID                           | `1`                                                                |
|  `MATOMO_URL`                  | Matomo Server URL                        | undef                                                              |
|  `MONGO_DB`                    | Rocket.Chat MongoDB Database             | `rocket`                                                           |
|  `MONGO_HOST`                  | Rocket.Chat MongoDB Host                 | `localhost`                                                        |
|  `MONGO_PASS`                  | Rocket.Chat MongoDB Password             | unset                                                              |
|  `MONGO_PORT`                  | Rocket.Chat MongoDB Port                 | `27017`                                                            |
|  `MONGO_USER`                  | Rocket.Chat MongoDB User                 | unset                                                              |
|  `OPENLDAP_BASE`               | OpenLDAP Base                            | seds `OPENLDAP_DOMAIN`, `example.com` produces `dc=example,dc=com` |
|  `OPENLDAP_BIND_DN_RREFIX`     | OpenLDAP Bind DN Prefix                  | `cn=rocket,ou=services`                                            |
|  `OPENLDAP_BIND_PW`            | OpenLDAP Bind Password                   | `secret`                                                           |
|  `OPENLDAP_CONF_DN_RREFIX`     | OpenLDAP Conf DN Prefix                  | `cn=lemonldap,ou=config`                                           |
|  `OPENLDAP_DOMAIN`             | OpenLDAP Domain Name                     | undef                                                              |
|  `OPENLDAP_HOST`               | OpenLDAP Backend Address                 | undef, enables LDAP integration                                    |
|  `OPENLDAP_PORT`               | OpenLDAP Bind Port                       | `389` or `636` depending on `OPENLDAP_PROTO`                       |
|  `OPENLDAP_PROTO`              | OpenLDAP Proto                           | `ldap`                                                             |
|  `PORT`                        | Rocket.Chat HTTP Port                    | `3000`                                                             |
|  `PUBLIC_PROTO`                | Rocket.Chat Public Proto                 | `http`                                                             |
|  `ROCKET_CLOUD_EMAIL`          | Rocket.Chat Push Notifications Email     | ``                                                                 |
|  `ROCKET_CLOUD_GATEWAY`        | Rocket.Chat Push Gateway                 | `https://gateway.rocket.chat`                                      |
|  `ROCKET_CLOUD_TOKEN`          | Rocket.Chat Push Notifications Token     | ``                                                                 |
|  `ROCKET_CLOUD_URL`            | Rocket.Chat Push Notifications Provider  | `https://cloud.rocket.chat`                                        |
|  `ROCKET_COUNTRY`              | Rocket.Chat Site Country                 | `france`                                                           |
|  `ROCKET_DOMAIN`               | Rocket.Chat Domain Name                  | `rocket.demo.local`                                                |
|  `ROCKET_INDUSTRY`             | Rocket.Chat Site Industry                | `blockchain`                                                       |
|  `ROCKET_INIT_MAX_RETRY`       | Wait Retries initializing Rocket.Chat    | `20` times                                                         |
|  `ROCKET_INIT_TIMER`           | Wait interval initializing Rocket.Chat   | `30` seconds                                                       |
|  `ROCKET_LANG`                 | Rocket.Chat Site Language                | `fr`                                                               |
|  `ROCKET_ORG_NAME`             | Rocket.Chat Site Organization            | `KubeChat Demo`                                                    |
|  `ROCKET_ORG_TYPE`             | Rocket.Chat Organization Type            | `enterprise`                                                       |
|  `ROCKET_ORG_URL`              | Rocket.Chat Organization URL             | `http://example.com`                                               |
|  `ROCKET_SAML_URL`             | Rocket.Chat SAML Entry Point Host (LLNG) | undef, enables LLNG integration                                    |
|  `ROCKET_SERVER_TYPE`          | Rocket.Chat Server Type                  | `privateTeam`                                                      |
|  `ROCKET_SITE_NAME`            | Rocket.Chat Site Name                    | `KubeChat`                                                         |
|  `ROCKET_WEB_MAX_RETRY`        | Wait Retries Rocket.Chat startup         | `60` times                                                         |
|  `ROCKET_WEB_TIMER`            | Wait interval Rocket.Chat startup        | `20` seconds                                                       |
|  `ROOT_URL`                    | Rocket.Chat Chat URL                     | `${PUBLIC_PROTO}://${ROCKET_DOMAIN}`                               |
|  `SMTP_FROM`                   | Rocket.Chat SMTP From                    | `rocket@demo.local`                                                |
|  `SMTP_HOST`                   | Rocket.Chat SMTP Relay                   | undef                                                              |
|  `SMTP_IGNORE_TLS`             | Rocket.Chat Ignore SMTP TLS Errors       | undef / no                                                         |
|  `SMTP_PORT`                   | Rocket.Chat SMTP Port                    | `25`                                                               |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                    |
| :------------------ | ------------------------------ |
|  `/saml`            | LemonLDAP SAML Certificates    |
|  `/certs`           | Custom PKI (openldap)          |

Setup Notes
------------

Init process would wait for `ROCKET_WEB_MAX_RETRY * ROCKET_WEB_TIMER` seconds
top, then we would try initializing its database - assuming admin user,
initial schemas and data were provisioned. Initialization may run up to
`ROCKET_INIT_MAX_RETRY` times, with a retry every `ROCKET_INIT_TIMER` seconds.
By default, we would wait up to 5 minutes for RocketChat web server startup,
then up to 8 minutes completing site initialization and eventually configuring
SAML authentication.

To figure out what was done during Rocket.Chat init process, here's how we would proceed:

- boot Rocket.Chat with `ADMIN_USERNAME` and `ADMIN_PASSWORD` set. This would
  initialize Rocket.Chat database, provisioning our initial admin user
- connect to MongoDB, dump the database

```
$ oc rsh sts/rocket-mongodb
sh$ cd /var/lib/mongodb
sh$ mkdir dump
sh$ mongodump --host 127.0.0.1 --username $MONGODB_USER --password $MONGODB_PASSWORD --db $MONGODB_DATABASE -o ./
sh$ cd ./<rocket-db-name>
sh$ ls *bson | while read line; do bsondump $line >$(echo $line | sed 's|\.bson|.json|'); done
sh$ rm *.bson *.metadata.json
sh$ find . -size 0 -exec rm -f {} \;
sh$ exit
$ oc cp rocket-mongodb-0:/var/lib/mongodb/dump/<rocket-db-name> dump
```

- next, connect Rocket.Chat with admin credentials and finish initializing your
  instance.
- back to MongoDB, dump the database again
- compare dumps pre and post-install. Most likely, only the `rocket_settings`
  would be relevant, though we could play with roles and channels as well.
  beware there's a shitload of internal IDs, update and insert dates along our
  records: better sed them out before looking at your diffs.
