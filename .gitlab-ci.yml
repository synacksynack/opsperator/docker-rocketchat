image: registry.gitlab.com/gdunstone/docker-buildx-qemu

services:
  - name: docker:dind
    entrypoint: ["env", "-u", "DOCKER_HOST"]
    command: ["dockerd-entrypoint.sh"]

stages:
  - build
  - test
  - release

variables:
  CI_BUILD_ARCHS_WORKS: linux/amd64
  CI_BUILD_ARCHS: linux/arm64/v8,linux/amd64
  DOCKER_DRIVER: overlay2
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_TLS_CERTDIR: ""
  IMAGE_ADDR: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  IMAGE_CACHE: $CI_REGISTRY_IMAGE/cache:$CI_COMMIT_SHA
  IMAGE_FINAL: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME

build:
  stage: build
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - update-binfmts --enable
    - docker buildx create --driver docker-container --use
    - docker buildx inspect --bootstrap
  script:
    - >
        docker buildx build --platform $CI_BUILD_ARCHS \
            --cache-from=type=registry,ref=$IMAGE_CACHE \
            --cache-to=type=registry,ref=$IMAGE_CACHE \
            --build-arg=DO_UPGRADE=yes \
            --progress plain --pull --push \
            -t "$IMAGE_ADDR" .

test:
  stage: test
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  image: docker:19.03.12
  retry:
    max: 2
  script:
    - >
        docker run --name testmongodb \
            --add-host=mongodb-0:127.0.0.1 \
            --entrypoint=container-entrypoint \
            --hostname=mongodb-0 \
            -e CI_MOCK=yay \
            -e DEBUG=yay \
            -e HOSTNAME=mongodb-0 \
            -e MONGODB_ADMIN_PASSWORD=mgadminpassword \
            -e MONGODB_DATABASE=mgdb \
            -e MONGODB_EXPOSE_LOCAL=yay \
            -e MONGODB_KEYFILE_VALUE=thisismymgkeyfilevalue \
            -e MONGODB_PASSWORD=mgpassword \
            -e MONGODB_REPLICA_NAME=thisismymgreplicaname \
            -e MONGODB_SERVICE_NAME=mongodb-0 \
            -e MONGODB_USER=mguser \
            -p 27017:27017 \
            -d registry.gitlab.com/synacksynack/opsperator/docker-mongodb:master \
            /usr/bin/run-mongod-replication
    - docker inspect testmongodb
    - >
        docker inspect testmongodb | awk '/"IPAddress": "/{print $2}'
    - >
        docker inspect testmongodb | awk '/"IPAddress": "/{print $2}' | head -1
    - >
        docker inspect testmongodb | awk '/"IPAddress": "/{print $2}' | head -1 | cut -d'"' -f2
    - >
        mgip=`docker inspect testmongodb | awk '/"IPAddress": "/{print $2}' | head -1 | cut -d'"' -f2`;
        echo "Working with mongodb=$mgip";
        echo "MONGO_HOST=$mgip" >.env;
    - >
        cpt=0;
        while true;
        do
            if docker exec testmongodb /bin/sh -c /usr/bin/is-master.sh; then
                break;
            elif test $cpt -ge 10; then
                exit 1;
            fi;
            echo Waiting for MongoDB to start ...;
            cpt=`expr $cpt + 1`;
            sleep 10;
        done
    - >
        docker run --name testrocketchat \
            --env-file=./.env \
            -e ADMIN_PASS=secret42secret42 \
            -e ADMIN_USER=rktadmin \
            -e DEBUG=yay \
            -e MONGO_DB=mgdb \
            -e MONGO_PASS=mgpassword \
            -e MONGO_REPLICA_NAME=thisismymgreplicaname \
            -e MONGO_USER=mguser \
            -e NODE_ENV=production \
            -e ROCKET_COUNTRY=france \
            -e ROCKET_DOMAIN=chat.demo.local \
            -e ROCKET_INDUSTRY=blockchain \
            -e ROCKET_LANG=en \
            -e ROCKET_ORG_NAME=GitLab \
            -e ROCKET_SERVER_TYPE=privateTeam \
            -e ROCKET_SITE_NAME=GitLabCi \
            -e ROCKET_INIT_MAX_RETRY=60 \
            -e ROCKET_WEB_MAX_RETRY=60 \
            -d $IMAGE_ADDR
    - sleep 360
    - >
        works=false;
        for try in 1 2 3 4 5 6 7 8 9 10;
        do
            echo "=== Test $try/10 ==="
            if docker exec testrocketchat /bin/bash -c 'curl http://127.0.0.1:3000/api/info' 2>&1 | grep ',"success":true}'; then
                works=true;
                break;
            elif test $try -eq 10; then
                break;
            fi;
            echo retrying in 30 seconds;
            sleep 30;
        done;
        echo "=== Container Logs ===";
        docker logs testrocketchat;
        echo "=== Cleaning Up ===";
        docker rm -f testrocketchat;
        docker rm -f testmongodb;
        if $works; then
            echo "Test: OK, RocketChat starts and listens";
        else
            echo "Test: KO, RocketChat does not answer";
            exit 1;
        fi

scan:
  stage: test
  image:
    name: docker.io/aquasec/trivy
    entrypoint: [""]
  script:
    - trivy image "$IMAGE_ADDR" || echo too-bad

retag:
  stage: release
  image:
    name: quay.io/skopeo/stable
    entrypoint: [""]
  script:
    - >
        if test "$CI_COMMIT_TAG"; then
            skopeo copy --all \
                --src-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                --dest-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                docker://$IMAGE_ADDR docker://$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
        else
            skopeo copy \
                --dest-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                --src-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                docker://$IMAGE_ADDR docker://$IMAGE_FINAL
        fi

release-code:
  rules:
    - if: $CI_COMMIT_TAG
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: '$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_TAG'
    tag_name: '$CI_COMMIT_TAG'
  script:
    - echo 'Releasing $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG'
