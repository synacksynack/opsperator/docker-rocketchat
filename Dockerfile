FROM docker.io/node:12.22.7-buster-slim

# Rocket.Chat image for OpenShift Origin

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive \
    RC_VERSION=3.18.3

LABEL io.k8s.description="Rocket.Chat is a NodeJS & MongoDB based chat solution." \
      io.k8s.display-name="Rocket.Chat ${RC_VERSION}" \
      io.openshift.expose-services="3000:http" \
      io.openshift.tags="chat,rocket,rocketchat,rocketchat3" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-rocket" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="${RC_VERSION}"

USER root

RUN set -ex \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get -y update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && if test "$DEBUG"; then \
	echo "# Install Debugging Tools" \
	&& apt-get -y install vim; \
    fi \
    && echo "# Install RocketChat dependencies" \
    && apt-get install --no-install-recommends --no-install-suggests -y \
	curl libnss-wrapper ldap-utils ca-certificates dumb-init \
	libmime-tools-perl bsdtar \
    && mkdir -p /app/uploads \
    && echo "# Fixing permissions" \
    && for i in /etc/ssl /usr/local/share/ca-certificates; \
	do \
	    chown -R 1001:root "$i" \
	    && chmod -R g=u "$i"; \
	done \
    && ( chmod a+rwx -R /run /tmp /app || echo nevermind ) \
    && echo "# Cleaning up" \
    && apt-get purge -y --auto-remove -o \
	APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/man /usr/share/doc

COPY config/* /

RUN set -x \
    && echo "# Install RocketChat ${RC_VERSION}" \
    && cd /app \
    && if uname -m | grep aarch64 >/dev/null; then \
	apt-get -y update \
	&& apt-get install --no-install-recommends --no-install-suggests -y \
	    python make gcc g++; \
    fi \
    && curl -fSL -o rocket.tar \
	"https://releases.rocket.chat/$RC_VERSION/download" \
    && bsdtar -xf rocket.tar \
    && cd bundle/programs/server \
    && mv /myldapsearch /usr/bin/ \
    && npm install \
    && npm cache clear --force \
    && echo "# Fixing permissions" \
    && chown -R 1001:root /app \
    && chmod -R g=u /app \
    && echo "# Cleaning up" \
    && rm -f /app/rocket.tar /app/bundle/README* \
    && if uname -m | grep aarch64 >/dev/null; then \
	apt-get -y remove --purge python make gcc g++ \
	&& apt-get purge -y --auto-remove -o \
	    APT::AutoRemove::RecommendsImportant=false \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/* /usr/share/man /usr/share/doc; \
    fi \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

WORKDIR /app/bundle
CMD [ "node", "main.js" ]
ENTRYPOINT [ "dumb-init","--","/run-rocket.sh" ]
USER 1001
