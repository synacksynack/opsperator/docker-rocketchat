#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

if test "`id -u`" -ne 0; then
    sed "s|^rocketchat:.*|rocketchat:x:`id -g`:|" /etc/group \
	>/var/run/rocket-group
    sed \
	"s|^rocketchat:.*|rocketchat:x:`id -u`:`id -g`:rocketchat:/var/www:/usr/sbin/nologin|" \
	/etc/passwd >/var/run/rocket-passwd
    export NSS_WRAPPER_PASSWD=/var/run/rocket-passwd
    export NSS_WRAPPER_GROUP=/var/run/rocket-group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi

MONGO_DB="${MONGO_DB:-rocket}"
MONGO_PORT=${MONGO_PORT:-27017}
ROCKET_DOMAIN="${ROCKET_DOMAIN:-rocket.demo.local}"

export Accounts_AvatarStorePath=/app/uploads
export DEPLOY_METHOD=docker-official
export HOME=/tmp
if test "$ADMIN_PASS"; then
    export ADMIN_PASS
    export ADMIN_USERNAME="${ADMIN_USERNAME:-rocket}"
fi
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
if test -z "$ROOT_URL"; then
    ROOT_URL="$PUBLIC_PROTO://$ROCKET_DOMAIN"
fi
export PORT=${PORT:-3000}
export ROOT_URL
should_rehash=false
if test "$MATOMO_URL"; then
    if ! echo "$MATOMO_URL" | grep -E '/$' >/dev/null; then
	MATOMO_URL="$MATOMO_URL/"
    fi
fi
for f in $(find /certs /run/secrets/kubernetes.io/serviceaccount -name '*.crt' 2>/dev/null)
do
    if test -s $f; then
	d=`echo $f | sed 's|/|-|g'`
	if ! test -s /usr/local/share/ca-certificates/kube$d.crt; then
	    if ! cat $f >/usr/local/share/ca-certificates/kube$d.crt; then
		echo WARNING: failed installing $f certificate authority >&2
	    else
		should_rehash=true
	    fi
	fi
    fi
done
if $should_rehash; then
    if ! update-ca-certificates; then
	echo WARNING: failed updating trusted certificate authorities >&2
    fi
fi
unset should_rehash

if test -z "$MONGO_URL"; then
    MONGO_HOST="${MONGO_HOST:-127.0.0.1}"
    MONGO_URL="mongodb://"
    hostList=
    if test "$MONGO_USER" -a "$MONGO_PASS"; then
	MONGO_URL="$MONGO_URL$MONGO_USER:$MONGO_PASS@"
    fi
    if test "$MONGODB_HOST_BASENAME"; then
	idx=0
	echo Listing MongoDB endpoints ...
	while :
	do
	    if ! getent hosts $MONGODB_HOST_BASENAME-$idx.$MONGODB_HOST_BASENAME \
		>/dev/null; then
		break
	    fi
	    hostList="$hostList $MONGODB_HOST_BASENAME-$idx.$MONGODB_HOST_BASENAME"
	    idx=`expr $idx + 1`
	done
    else
	hostList=$MONGO_HOST
    fi
    hasHost=""
    for host in $hostList
    do
	if test "$hasHost"; then
	    hasHost="$hasHost,"
	fi
	hasHost="$hasHost$host:$MONGO_PORT"
    done
    MONGO_URL="$MONGO_URL$hasHost"
    if test -z "$MONGO_REPLICA_NAME"; then
	MONGO_URL="$MONGO_URL/$MONGO_DB"
    else
	if test -z "$MONGO_OPLOG_URL"; then
	    MONGO_OPLOG_URL="$MONGO_URL/local?authSource=admin"
	fi
	if test -z "$MONGO_HOST_BASENAME"; then
	    MONGO_URL="$MONGO_URL/$MONGO_DB?replSet=$MONGO_REPLICA_NAME"
	else
	    MONGO_OPLOG_URL="$MONGO_OPLOG_URL&slaveOk=true"
	    MONGO_URL="$MONGO_URL/$MONGO_DB?replSet=$MONGO_REPLICA_NAME&slaveOk=true"
	fi
	export MONGO_OPLOG_URL
    fi
fi
export MONGO_URL
export USE_NATIVE_OPLOG=true

if test -z "$DONT_WAIT_FOR_MONGO"; then
    cpt=0
    echo Waiting for MongoDB service ...
    testHost=$(echo $MONGO_URL | cut -d/ -f3 | sed -e "s|^.*@||" -e "s|:.*$||" | cut -d, -f1)
    while ! bash -c \
	    "echo dummy >/dev/tcp/$testHost/$MONGO_PORT" \
	    >/dev/null 2>&1
    do
	if test "$cpt" -gt 10; then
	    echo MongoDB unavailable, timeout reached, exiting >&2
	    exit 1
	fi
	cpt=`expr $cpt + 1`
	sleep 5
	echo MongoDB is ... KO
    done
    echo MongoDB OK
fi

if test "$OPENLDAP_HOST"; then
    OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=rocket,ou=services}"
    OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
    OPENLDAP_CONF_DN_PREFIX="${OPENLDAP_CONF_DN_PREFIX:-ou=lemonldap,ou=config}"
    OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
    OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
    if test -z "$OPENLDAP_BASE"; then
	OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
    fi
    if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
	OPENLDAP_PORT=636
    elif test -z "$OPENLDAP_PORT"; then
	OPENLDAP_PORT=389
    fi
    export LLNG_CLIENT_CRT=/tmp/saml-client.crt
    export LLNG_CLIENT_KEY=/tmp/saml-private.key
    export LLNG_SERVER_CRT=/tmp/saml-public.crt
    export OPENLDAP_HOST OPENLDAP_BIND_PW OPENLDAP_BIND_DN_PREFIX \
	OPENLDAP_BASE OPENLDAP_PORT OPENLDAP_PROTO
    echo Waiting for LDAP backend ...
    cpt=0
    while true
    do
	if LDAPTLS_REQCERT=never \
		ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
		-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		-b "ou=users,$OPENLDAP_BASE" \
		-w "$OPENLDAP_BIND_PW" \
		"(objectClass=inetOrgPerson)" >/dev/null 2>&1; then
	    echo " LDAP is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo Could not reach OpenLDAP >&2
	    exit 1
	fi
	sleep 5
	echo LDAP is ... KO
	cpt=`expr $cpt + 1`
    done
    if test "$ROCKET_SAML_URL"; then
	echo Waiting for SAML backend ...
	cpt=0
	while true
	do
	    if curl -k "$ROCKET_SAML_URL" >/dev/null 2>&1; then
		echo " LemonLDAP is alive!"
		break
	    elif test "$cpt" -gt 25; then
		echo Could not reach LemonLDAP >&2
		exit 1
	    fi
	    sleep 5
	    echo SAML is ... KO
	    cpt=`expr $cpt + 1`
	done
	if test -s /saml/lemon.crt; then
	    cat /saml/lemon.crt >"$LLNG_SERVER_CRT"
	elif ! LDAPTLS_REQCERT=never myldapsearch \
		-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		-w "$OPENLDAP_BIND_PW" \
		-b "$OPENLDAP_CONF_DN_PREFIX,$OPENLDAP_BASE" \
		-H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT/ \
		| grep -A32 '^description:: {samlServicePublicKeySig}' \
		| sed 's|description:: {samlServicePublicKeySig}||' \
		| awk 'BEG{ took=0; }{ if (took != 0) { if ($0 == "" || $0 ~ /description/) { exit 0; }} print $0;took=1; }' \
		>"$LLNG_SERVER_CRT" 2>/dev/null; then
	    mv "$LLNG_SERVER_CRT" "$LLNG_SERVER_CRT.failed"
	    cat <<EOF >&2
WARNING: Failed querying $OPENLDAP_PROTO://$OPENLDAP_HOST for SAML signing certificate
WARNING: may not be able to authenticate SAML users
EOF
	fi
	if test -s "$LLNG_SERVER_CRT"; then
	    if test -s /saml/tls.crt -a -s /saml/tls.key; then
		cat /saml/tls.crt >"$LLNG_CLIENT_CRT"
		cat /saml/tls.key >"$LLNG_CLIENT_KEY"
	    else
		openssl req -nodes -new -x509 -days 3650 \
		    -keyout "$LLNG_CLIENT_KEY" -out "$LLNG_CLIENT_CRT" \
		    -subj "/CN=$ROCKET_DOMAIN"
	    fi
	    if ! test -s "$LLNG_CLIENT_CRT"; then
		cat <<EOF >&2
WARNING: Failed generating SAML client certificate
WARNING: may not be able to authenticate SAML users
EOF
	    fi
	fi
    fi
fi

if test -s /hack-init.js -a "$ADMIN_PASS" -a "$ADMIN_USERNAME"; then
    (
	cpt=0
	max=${ROCKET_WEB_MAX_RETRY:-60}
	w8t=${ROCKET_WEB_TIMER:-20}
	while ! curl --connect-timeout 5 \
	    http://127.0.0.1:$PORT/ 2>&1 | grep -Ei '(rocket|meteor)' >/dev/null
	do
	    echo "Waiting for RocketChat to startup ($cpt / $max)..."
	    sleep $w8t
	    cpt=`expr $cpt + 1`
	    if test "$cpt" -ge "$max"; then
		echo "WARING: still no response from webapp, trying anyway" >&2
		break
	    fi
	done
	if test "$ROCKET_SAML_URL" -a -z "$ROCKET_ORG_URL"; then
	    ROCKET_ORG_URL="$ROCKET_SAML_URL"
	fi
	cpt=0
	max=${ROCKET_INIT_MAX_RETRY:-20}
	w8t=${ROCKET_INIT_TIMER:-30}
	while test "$cpt" -lt "$max"
	do
	    echo "Trying to initialize RocketChat ($cpt / $max)..."
	    node /hack-init.js && break
	    cpt=`expr $cpt + 1`
	    if test "$cpt" -lt "$max"; then
		echo "... retrying in ${w8t}s"
	    else
		echo "CRITICAL: site initialization failed"
		break
	    fi
	    sleep $w8t
	done
	if test "$cpt" -lt "$max"; then
	    rm -f "$LLNG_CLIENT_KEY" "$LLNG_SERVER_CRT" "$LLNG_CLIENT_CRT"
	fi
    ) &
fi

echo "Starting Rocket.Chat $RC_VERSION against $MONGO_URL" \
    | sed "s|$MONGO_USER:$MONGO_PASS@||g"
unset OPENLDAP_HOST OPENLDAP_BIND_DN_PREFIX OPENLDAP_CONF_DN_PREFIX \
    OPENLDAP_BIND_PW OPENLDAP_BASE OPENLDAP_PORT OPENLDAP_PROTO PUBLIC_PROTO \
    ROCKET_DOMAIN DONT_WAIT_FOR_MONGO ROCKET_SAML_URL MONGO_DB MONGO_PORT \
    MONGO_USER MONGO_PASS ROCKET_INIT_TIMER ROCKET_INIT_MAX_RETRY max w8t \
    cpt LLNG_CLIENT_CRT LLNG_SERVER_CRT LLNG_CLIENT_KEY ROCKET_ORG_URL \
    ROCKET_WEB_TIMER ROCKET_WEB_MAX_RETRY testHost hostList

exec "$@"
