'use strict';

/*
 * FIXME:
 * maybe don't reset SAML configuration on every boot
 * if all 3 certs fields are initialized, don't overwrite them
 */

let MongoClient;
try { MongoClient = require('/app/bundle/programs/server/npm/node_modules/mongodb').MongoClient; }
catch(e) { MongoClient = require('/app/bundle/programs/server/npm/node_modules/meteor/npm-mongo/node_modules/mongodb').MongoClient; }

function getEnv(v, dflt) {
    if (process.env[v] !== undefined) {
	if (process.env[v] !== '') {
	    return process.env[v];
	}
    }
    return dflt !== undefined ? dflt : false;
}

const dbName = process.env.MONGO_DB || 'rocket';
const debug = getEnv('DEBUG');
const fs = require('fs');
const settingsCollection = 'rocketchat_settings';
const url = getEnv('MONGO_URL', `mongdb://127.0.0.1:27017/${dbName}`);

let ldapCACert = false, ldapLogin = false, samlLogin = false, samlLogout = false, samlMeta = false,
    priv = false, idpcrt = false, pub = false;

if (getEnv('ROCKET_SAML_URL')) {
    samlLogin  = `${process.env.ROCKET_SAML_URL}/saml/singlesignon`;
    samlLogout = `${process.env.ROCKET_SAML_URL}/saml/singlelogout`;
    samlMeta   = `${process.env.ROCKET_SAML_URL}/saml/metadata`;
    try {
	idpcrt = fs.readFileSync(process.env.LLNG_SERVER_CRT, 'utf8');
	priv   = fs.readFileSync(process.env.LLNG_CLIENT_KEY, 'utf8');
	pub    = fs.readFileSync(process.env.LLNG_CLIENT_CRT, 'utf8');
    } catch(e) {
	pub = priv = idpcrt = '';
	console.log('could not load SAML certificates - continuing without SAML');
    }
}

if (getEnv('OPENLDAP_HOST')) {
    ldapLogin = true;
    try {
	ldapCACert = fs.readFileSync(`/certs/ca.crt`, 'utf8');
	ldapCACert.replace("\n", '\\n')
    } catch(e) {
	ldapCACert = "";
	console.log('could not load LDAP CA certificate');
    }
}

const cloudValues = [
	{ key: 'Cloud_Service_Agree_PrivacyTerms', value: 'true' },
	{ key: 'Cloud_Url', value: getEnv('ROCKET_CLOUD_URL', 'https://cloud.rocket.chat') },
	{ key: 'Cloud_Workspace_Client_Id', value: getEnv('ROCKET_CLOUD_EMAIL', '') },
	{ key: 'Cloud_Workspace_Client_Secret', value: getEnv('ROCKET_CLOUD_TOKEN', '') },
// or Cloud_Workspace_Access_Token ?
	{ key: 'Cloud_Workspace_Name', value: getEnv('ROCKET_SITE_NAME', 'KubeChat') },
	{ key: 'Push_enable', value: (getEnv('ROCKET_CLOUD_EMAIL') && getEnv('ROCKET_CLOUD_TOKEN')) ? 'true' : 'false' },
	{ key: 'Push_enable_gateway', value: (getEnv('ROCKET_CLOUD_EMAIL') && getEnv('ROCKET_CLOUD_TOKEN')) ? 'true' : 'false' },
	{ key: 'Push_gateway', value: getEnv('ROCKET_CLOUD_GATEWAY', 'https://gateway.rocket.chat') },
	{ key: 'Register_Server', value: (getEnv('ROCKET_CLOUD_EMAIL') && getEnv('ROCKET_CLOUD_TOKEN')) ? 'true' : 'false' },
	{ key: 'Update_EnableChecker', value: 'false' }
    ];
const initValues = [
	{ key: 'Allow_Marketing_Emails', value: 'false' },
	{ key: 'Accounts_SystemBlockedUsernameList', value: 'administrator,system,user' },
	{ key: 'Accounts_TwoFactorAuthentication_By_Email_Auto_Opt_In', value: 'false' },
	{ key: 'Accounts_TwoFactorAuthentication_By_Email_Enabled', value: 'false' },
	{ key: 'Accounts_TwoFactorAuthentication_Enabled', value: 'false' },
	{ key: 'Accounts_TwoFactorAuthentication_Enforce_Password_Fallback', value: 'false' },
	{ key: 'Country', value: getEnv('ROCKET_COUNTRY', 'france') },
	{ key: 'From_Email', value: getEnv('SMTP_FROM', 'rocket@demo.local') },
	{ key: 'Industry', value: getEnv('ROCKET_INDUSTRY', 'blockchain') },
	{ key: 'Language', value: getEnv('ROCKET_LANG', 'fr') },
	{ key: 'Organization_Name', value: getEnv('ROCKET_ORG_NAME', 'KubeChat Demo') },
	{ key: 'Organization_Type', value: getEnv('ROCKET_ORG_TYPE', 'enterprise') },
	{ key: 'Server_Type', value: getEnv('ROCKET_SERVER_TYPE', 'privateTeam') },
	{ key: 'Site_Name', value: getEnv('ROCKET_SITE_NAME', 'KubeChat') },
	{ key: 'Site_Url', value: getEnv('ROOT_URL', 'http://localhost:3000') },
	{ key: 'Size', value: '0' },
	{ key: 'SMTP_Host', value: getEnv('SMTP_HOST', '') },
	{ key: 'SMTP_IgnoreTLS', value: getEnv('SMTP_IGNORE_TLS') ? 'true' : 'false' },
	{ key: 'SMTP_Port', value: getEnv('SMTP_PORT', '25') },
	{ key: 'SMTP_Protocol', value: getEnv('SMTP_PROTO', 'smtp') },
	{ key: 'Website', value: getEnv('ROCKET_ORG_URL', getEnv('ROCKET_SAML_URL', 'http://example.com')) }
    ];
const ldapValues = [
	{ key: 'LDAP_Authentication', value: 'true' },
	{ key: 'LDAP_Authentication_Password', value: getEnv('OPENLDAP_BIND_PW', '') },
	{ key: 'LDAP_Authentication_UserDN', value: getEnv('OPENLDAP_BIND_DN_PREFIX', '') + ',' + getEnv('OPENLDAP_BASE', '') },
	{ key: 'LDAP_Background_Sync', value: 'true' },
	{ key: 'LDAP_Background_Sync_Import_New_Users', value: 'true' },
	{ key: 'LDAP_Background_Sync_Interval', value: 'Every 24 hours' },
	{ key: 'LDAP_Background_Sync_Keep_Existant_Users_Updated', value: 'true' },
	{ key: 'LDAP_BaseDN', value: getEnv('OPENLDAP_BASE', '') },
	{ key: 'LDAP_CA_Cert', value: ldapCACert },
	{ key: 'LDAP_Enable', value: 'true' },
	{ key: 'LDAP_Encryption', value: process.env.OPENLDAP_PROTO === 'ldaps' ? 'ssl' : 'plain' },
	{ key: 'LDAP_Find_User_After_Login', value: 'false' },
	{ key: 'LDAP_Host', value: process.env.OPENLDAP_HOST || '' },
	{ key: 'LDAP_Internal_Log_Level', value: debug ? 'debug' : 'info' },
	{ key: 'LDAP_Login_Fallback', value: 'true' },
	{ key: 'LDAP_Merge_Existing_Users', value: 'true' },
	{ key: 'LDAP_Port', value: process.env.OPENLDAP_PORT || '' },
	{ key: 'LDAP_Unique_Identifier_Field', value: 'uid' },
	{ key: 'LDAP_User_Search_Field', value: 'uid' },
	{ key: 'LDAP_User_Search_Filter', value: `(&(objectClass=inetOrgPerson)(!(pwdAccountLockedTime=*)))` },
	{ key: 'LDAP_Username_Field', value: 'uid' },
	{ key: 'LDAP_Sync_User_Data', value: 'true' },
	{ key: 'LDAP_Sync_User_Data_FieldMap', value: `{"uid":"name","mail":"email"}` },
	{ key: 'LDAP_Sync_User_Data_Groups', value: 'true' },
	{ key: 'LDAP_Sync_User_Data_Groups_AutoRemove', value: 'true' },
	{ key: 'LDAP_Sync_User_Data_Groups_BaseDN', value: 'ou=groups,' +  getEnv('OPENLDAP_BASE', '') },
	{ key: 'LDAP_Sync_User_Data_Groups_Filter', value: '(&(cn=#{groupName})(member=uid=#{username},ou=users,' + getEnv('OPENLDAP_BASE', '') + '))' },
	{ key: 'LDAP_Sync_User_Data_GroupsMap', value: '{"Admins":"admin"}' }
    ];
const matomoValues = [
	{ key: 'PiwikAdditionalTrackers', value: getEnv('MATOMO_ADDITIONAL_TRACKERS', '') },
	{ key: 'PiwikAnalytics_cookieDomain', value: getEnv('MATOMO_COOKIE_DOMAIN', '') },
	{ key: 'PiwikAnalytics_domains', value: getEnv('MATOMO_DOMAINS', '') },
	{ key: 'PiwikAnalytics_enabled', value: 'true' },
	{ key: 'PiwikAnalytics_prependDomain', value: getEnv('MATOMO_PREPEND_DOMAIN', '') },
	{ key: 'PiwikAnalytics_siteId', value: getEnv('MATOMO_SITE_ID', '1') },
	{ key: 'PiwikAnalytics_url', value: getEnv('MATOMO_URL', '') }
    ];
const prometheusValues = [
	{ key: 'Prometheus_Enabled', value: 'true' },
	{ key: 'Prometheus_Port', value: getEnv('PROMTHEUS_PORT', '9113') }
    ];
const samlValues = [
	{ key: 'SAML_Custom_Default_button_label_text', value: 'Login with SSO' },
	{ key: 'SAML_Custom_Default_debug', value: debug ? 'true' : 'false' },
	{ key: 'SAML_Custom_Default_entry_point', value: samlLogin },
	{ key: 'SAML_Custom_Default_generate_username', value: 'true' },
	{ key: 'SAML_Custom_Default_idp_slo_redirect_url', value: samlLogout },
	{ key: 'SAML_Custom_Default_issuer', value: samlMeta },
	{ key: 'SAML_Custom_Default_logout_behaviour', value: 'SAML' },
	{ key: 'SAML_Custom_Default_mail_overwrite', value: 'true' },
	{ key: 'SAML_Custom_Default_name_overwrite', value: 'true' },
	{ key: 'SAML_Custom_Default_user_data_fieldmap', value: '{"email":"email","name":"uid"}' },
	{ key: 'SAML_Custom_Default_cert', value: idpcrt },
	{ key: 'SAML_Custom_Default_public_cert', value: pub },
	{ key: 'SAML_Custom_Default_private_key', value: priv },
	{ key: 'SAML_Custom_Default_provider', value: 'rocket' }
    ];

const closeClient = (c) => {
	if (c !== undefined) {
	    try { c.close(); }
	    catch(q) {}
	}
    };

const initDone = (c, msg, code) => {
	closeClient(c);
	console.log(msg || 'done applying post-initialization tasks');
	process.exit(code || 0);
    };
const initFail = (c, e) => {
	closeClient(c);
	console.log('failed initializing');
	console.log(e || 'undefined error');
	process.exit(1);
    };

const patchRecord = (db, s) => {
	return new Promise((resolve, reject) => {
		try {
		    let cast;
		    if (s.value == 'true' || s.value == 'false') {
			cast = s.value == 'true';
		    } else { cast = s.value; }
		    if (s.value === '') {
			if (debug !== undefined) {
			    console.log(`skipping ${s.key} - null value`);
			}
			resolve({});
		    } else {
			if (debug !== undefined) {
			    console.log(`should patch ${s.key} with ${cast}`);
			}
			db.collection(settingsCollection)
			    .updateOne({ '_id': s.key },
				    { $set: { value: cast } },
				    (err, r) => {
					    if (err !== null) { reject(err); }
					    else { resolve(r); }
					});
		    }
		} catch(e) { reject(e); }
	    });
    };
const checkRecord = (db, s, strictCheck) => {
	return new Promise((resolve, reject) => {
	    db.collection(settingsCollection)
		.find({ '_id': s.key })
		.toArray((e, d) => {
			if (e !== null) { reject(e); }
			else if (d.length === 0) {
			    console.log(`WARNING: failed resolving ${s.key}`);
			    if (strictCheck !== undefined) {
				reject('not found');
			    } else { resolve(false); }
			} else {
			    console.log(`checking ${s.key} configuration`);
			    if (debug) {
				console.log(`has ${s.key}=${d[0].value}`);
				console.log(`want ${s.key}=${s.value}`);
			    }
			    if (s.key === 'Show_Setup_Wizard'
				    && d[0].value === 'pending') {
				if (debug) { console.log('waiting for init'); }
				reject('try again later');
			    } else if (String(d[0].value) !== String(s.value)) {
				patchRecord(db, s)
				    .then((r) => {
					    console.log(`${s.key} patched`);
					    resolve(r);
					})
				    .catch((r) => reject(r));
			    } else {
				if (debug) { console.log(`ignoring ${s.key}`); }
				resolve(false);
			    }
			}
		    });
	});
    };

console.log("Initializing RocketChat site configuration");
if (debug !== undefined) { console.log(url); }

try {
    MongoClient.connect(url, (err, client) => {
	    if (err !== null) {
		console.log('Failed connecting to MongoDB');
		process.exit(1);
	    }
	    console.log("Connected successfully to server");

	    try {
		const db = client.db(dbName);
		let async = [], setting, i;
		for (i = 0; i < initValues.length; i++) {
		    setting = initValues[i];
		    async.push(checkRecord(db, setting));
		}
		for (i = 0; i < cloudValues.length; i++) {
		    setting = cloudValues[i];
		    async.push(checkRecord(db, setting));
		}
		if (samlLogin !== false) {
		    for (i = 0; i < samlValues.length; i++) {
			setting = samlValues[i];
			async.push(checkRecord(db, setting));
		    }
		}
		if (ldapLogin !== false) {
		    for (i = 0; i < ldapValues.length; i++) {
			setting = ldapValues[i];
			async.push(checkRecord(db, setting));
		    }
		} else {
		    async.push(checkRecord(db, {
			    key: 'LDAP_Enabled',
			    value: 'false'
			}));
		}
		if (getEnv('MATOMO_URL')) {
		    for (i = 0; i < matomoValues.length ; i++) {
			setting = matomoValues[i];
			async.push(checkRecord(db, setting));
		    }
		} else {
		    async.push(checkRecord(db, {
			    key: 'PiwikAnalytics_enabled',
			    value: 'false'
			}));
		}
		if (getEnv('PROMETHEUS_ENABLED')) {
		    for (i = 0; i < prometheusValues.length ; i++) {
			setting = prometheusValues[i];
			async.push(checkRecord(db, setting));
		    }
		} else {
		    async.push(checkRecord(db, {
			    key: 'Prometheus_Enabled',
			    value: 'false'
			}));
		}
		checkRecord(db, {
			    key: 'Show_Setup_Wizard',
			    value: 'completed'
			}, true)
		    .then(() => Promise.all(async))
		    .then(() => checkRecord(db, {
					key: 'LDAP_Sync_Now',
					value: ldapLogin === false ? 'false' : 'true'
				    }))
		    .then(() => checkRecord(db, {
					key: 'SAML_Custom_Default',
					value: samlLogin === false ? 'false' : 'true'
				    }))
		    .then(() => initDone(client))
		    .catch((e) => initFail(client, e));
	    } catch(e) { initFail(client, e); }
	});
} catch(e) {
    console.log("Could not initiate link to MongoDB");
    process.exit(1);
}
